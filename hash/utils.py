class HashGenerator(object):
    def hash(self,s):
        h = 7
        letters = "acdegilmnoprstuw"
        for l in s:
            h = h * 37 + letters.index(l)

        return h

class ReverseHasher(object):
    def unhash(self,num):
        """
        unhash and return
        """
        letters = "acdegilmnoprstuw"
        original = ''
        while num != 7:
            rem = num % 37
            original = letters[rem] + original 
            num = num/37
        return original
