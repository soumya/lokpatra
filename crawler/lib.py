from bs4 import BeautifulSoup
from crawler.crawler_conf import *
import requests
import logging
import traceback
logging.basicConfig(filename='logs/lokpatra.log',level=logging.DEBUG)

class ShoppingCrawlerInterface(object):
    
    def __init__(self):
        self.num_query_url = "http://www.shopping.com/products?KW=%s"
        self.text_query_url = "http://www.shopping.com/products~PG-%s?KW=%s"

    def getCount(self, keyword):
        raise NotImplementedError("Not implemented")

    def getResults(self, num, keyword):
        raise NotImplementedError("Not implemented")
        

class ShoppingCrawlerMixin(object):

    def getContentFromPage(self,keyword):
        html_text = ''
        try:
            html_text = requests.get(self.num_query_url %(keyword)).content
            logging.info(
                    "%s\t%s\t%s\t%s" %
                    ("ShoppingCrawlerMixin.getContentFromPage",
                     "response for query",
                     "",
                     html_text 
                    )
            )

        except Exception, e:
            logging.critical(
                    "%s\t%s\t%s\t%s" %
                    ("ShoppingCrawlerMixin.getContentFromPage",
                     "error",
                     traceback.format_exc().replace('\n',''),
                     e.message
                    )
            )
        return html_text

    def getContentFromNumberedPage(self,keyword,page_num):
        html_text = ''
        try:
            html_text = requests.get(self.text_query_url %(keyword,page_num)).content
            logging.info(
                    "%s\t%s\t%s\t%s" %
                    ("ShoppingCrawlerMixin.getContentFromNumberedPage",
                     "response for query",
                     "",
                     html_text 
                    )
            )

        except Exception, e:
            logging.critical(
                    "%s\t%s\t%s\t%s" %
                    ("ShoppingCrawlerMixin.getContentFromNumberedPage",
                     "error",
                     traceback.format_exc().replace('\n',''),
                     e.message
                    )
            )
        return html_text

    def get_element_of_queryset(self,item_div_tag,count):
        item_text,item_price = '','NA'
        try:
            item_text = item_div_tag.find('div', {'class' : 'gridItemBtm'}).find('h2').find(id = "nameQA%s"%(count)).get('title')
            item_price = getattr(item_div_tag.find('div', {"class" : "gridItemBtm"}).find('span', {"class" : "productPrice"}), 'text', 'NA').strip('\n')
        except Exception,e :
            logging.critical(
                    "%s\t%s\t%s\t%s" %
                    ("ShoppingCrawlerMixin.get_element_of_queryset",
                     "error",
                     traceback.format_exc().replace('\n',''),
                     e.message
                    )
            )
         
        return item_text,item_price

    def query_result_item_generator(self):
        count = 0
        while True:
            count += 1
            item_div_tag = self.result_parent_container.find('div', {"id" : "quickLookItem-%s"%(count)})
            yield self.get_element_of_queryset(item_div_tag,count)
            
            
        
    def parsePageAndGetCount(self, **kwargs):
        count_box_text = self.soup.find('div', {"id": "sortFiltersBox"}).span['name']
        count = 0
        try:
            text,count_text = count_box_text.split(':')
            count = int(count_text)
        except Exception, e:
            logging.critical(
                    "%s\t%s\t%s\t%s" %
                    ("ShoppingCrawlerMixin.parsePageAndGetCount",
                     "error",
                     traceback.format_exc().replace('\n',''),
                     e.message
                    )
            )
            
        return count

class ShoppingCrawlerImpl(ShoppingCrawlerInterface, ShoppingCrawlerMixin):
    
    def __init__(self):
        self.soup = None
        super(ShoppingCrawlerImpl, self).__init__()

    def getCount(self,keyword):
        self.soup = BeautifulSoup(self.getContentFromPage(keyword), 'html.parser')
        return self.parsePageAndGetCount() 

    def getResults(self, keyword, page_num):
        self.soup = BeautifulSoup(self.getContentFromNumberedPage(keyword,page_num), 'html.parser')
        self.result_parent_container = self.soup.find('form', {"name" : 'compareprd'})
        item_generator = self.query_result_item_generator()
        item_list = []
        item_text, item_price = next(item_generator)
        while item_text:
            item_list.append((item_text, item_price))
            item_text, item_price = next(item_generator)

        return item_list
